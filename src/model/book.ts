export interface Book {
  id: number;
  author: string;
  name: string;
}
