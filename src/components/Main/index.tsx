import ListBooks from 'components/List';
import { Book } from 'model/book';
import { useEffect, useState } from 'react';
import api from 'services/api';

const Main = () => {
  const [response, setResponse] = useState([]);
  const [error, setError] = useState([]);

  const fetchData = () => {
    api
      .get('http://localhost:8080/api/book/find/all')
      .then((res) => {
        setResponse(res.data);
      })
      .catch((err) => setError(err.data));
  };

  useEffect(() => {
    fetchData();
  }, []);

  const listBooks = response.map((book: Book) => {
    <ListBooks key={book.id} id={0} author={''} name={''} />;
  });

  return <ul>{listBooks}</ul>;
};

export default Main;
