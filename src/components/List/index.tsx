import { Book } from 'model/book';

function ListBooks(book: Book) {
  return (
    <>
      <li>{book.id}</li>
      <li>{book.name}</li>
      <li>{book.author}</li>
    </>
  );
}

export default ListBooks;
